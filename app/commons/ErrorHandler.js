'use strict';
/**
 * Database Error
 */
const DataBaseError = require("../commons/models/errors/DataBaseError");
/**
 * Respuesta común
 */
const commonResponse = require('../commons/models/commonResponse');
/**
 * Headers de la respuesta
 */
const commons = require("../commons/models/commonHeaders");
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Handler para obtener la información de los items disponibles
 * @author          Hector Trujillo Ruiz
 * @creationDate    28 de Octubre del 2019
 * @class Handler que gestiona la información de los items disponibles
 */
let errorHandler = {};
/**
 * Método que obtiene la lista de items por tipo
 * @param {Error} error Error a evaluar
 * @returns {commonResponse}
 */
errorHandler.handleError = (error) => {
    console.error("Error found: ", error);
    let message = error.message, errorCode = 0;
    /// Sí se trata de un error de base de datos
    if (error instanceof DataBaseError) {
        // response.setBody(new Object({
        //     message: 'Internal Error'
        // }));
        message = 'Internal Error';
    }
    let dataError = new Object({
        message: message
    });
    if (errorCode > 0) {
        dataError.errorCode = errorCode;
    }
    /// Inicializa la respuesta por defecto
    // let response = new commonResponse(409, commons.responseHeaders, dataError);
    return new commonResponse(409, commons.responseHeaders, dataError);
};
/// Exporta el módulo
module.exports = errorHandler;
