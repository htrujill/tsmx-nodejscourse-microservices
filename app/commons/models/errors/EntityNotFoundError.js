/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase para el error de una entidad no encontrada en la base de datos
 * @author          Hector Trujillo Ruiz
 * @creationDate    10 de Noviembre del 2019
 * @class Clase para el error de una entidad no encontrada en la base de datos
 */
class EntityNotFoundError extends Error {
    /**
     * Constructor de la clase
     * @param {String} message Mensaje de error
     * @param {Object} stack Seguimiento del error
     */
    constructor(message, stack) {
        super(message);
        this.name = "EntityNotFoundError";
        this.stack = stack;
    }
}
/// Exporta la clase
module.exports = EntityNotFoundError;