/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que gestiona el error que indica que una operación no es soportada
 * @author          Hector Trujillo Ruiz
 * @creationDate    02 de Dicembre del 2019
 * @class           Clase que gestiona el error que indica que una operación no es soportada
 */
class UnsupportedOperation extends Error {
    /**
     * Constructor de la clase
     * @param {String} message Mensaje de error
     * @param {Object} stack Seguimiento del error
     */
    constructor(message, stack) {
        super(message);
        this.name = "UnsupportedOperation";
        this.stack = stack;
    }
}
/// Exporta la clase
module.exports = UnsupportedOperation;