/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que gestiona errores para cuando un valor no ha sido proporcionado
 * @author          Hector Trujillo Ruiz
 * @creationDate    15 de Noviembre del 2019
 * @class           Clase que gestiona errores para cuando un valor no ha sido proporcionado
 */
class UnauthorizedAccessError extends Error {
    /**
     * Constructor de la clase
     * @param {String} message Mensaje de error
     * @param {Object} stack Seguimiento del error
     */
    constructor(message, stack) {
        super(message);
        this.name = "UnauthorizedAccessError";
        this.stack = stack;
    }
}
/// Exporta la clase
module.exports = UnauthorizedAccessError;