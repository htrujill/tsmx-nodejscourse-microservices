/**
 * Funciones globales agregadas al prototype de los tipos de datos nativos
 */
const globalFunctiona = require('../functions/PrototypeFunctions');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase de base que contiene las propiedades comunes
 * @author          Hector Trujillo Ruiz
 * @creationDate    09 de Noviembre del 2019
 * @class           Clase de base que contiene las propiedades comunes
 */
class BaseEntity {
    /**
     * Constructor de la clase
     * @param {Number} id Id de la entidad
     * @param {String} comments Comentarios
     * @param {Date} lastUpdate Fecha de actualización
     * @param {String} updatedBy Usuario que actualiza la información
     * @param {Number} status Estado de la entidad
     */
    constructor(id, comments, lastUpdate, updatedBy, status) {
        if (id != null)
            this.id = id;
        if (comments != null)
            this.comments = comments;
        if (lastUpdate != null)
            this.lastUpdate = lastUpdate instanceof Date ? lastUpdate.getUnixTime() : lastUpdate;
        if (updatedBy != null)
            this.updatedBy = updatedBy;
        if (status != null)
            this.status = typeof status == 'number' ? status : (Buffer.from(status))[0];
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getComments() {
        return this.comments;
    }

    setComments(comments) {
        this.comments = comments;
    }

    getLastUpdate() {
        return this.lastUpdate;
    }

    getLastUpdateAsDate() {
        return new Date(this.lastUpdate * 1000);
    }

    setLastUpdate(lastUpdate) {
        this.lastUpdate = lastUpdate instanceof Date ? lastUpdate.getUnixTime() : lastUpdate;
    }

    getUpdatedBy() {
        return this.updatedBy;
    }

    setUpdatedBy(updatedBy) {
        this.updatedBy = updatedBy;
    }

	/**
	 * Obtiene el status de la ubicación
	 */
    getStatus() {
        return this.status;
    }

	/**
	 * Asigna el status de la ubicación
	 * @param {String} status status de la ubicación
	 */
    setStatus(status) {
        console.log(status);
        console.log(status instanceof Number);
        console.log(typeof status);
        this.status = typeof status == 'number' ? status : (Buffer.from(status))[0];
    }
}

module.exports = BaseEntity;