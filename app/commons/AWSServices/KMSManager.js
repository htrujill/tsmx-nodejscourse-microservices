/// Variable de AWS
const AWS = require('aws-sdk');
/// Variable de KMS
const KMS = new AWS.KMS(
    {
        endpoint: process.env.URL_KMS_VPC
    }
);
/// Inicializa el módulo a devolver
let KMSModule = {};
/**
 * Funcion que devuelve el texto de un parámetro encriptado por KMS
 * @param {String} Ciphertext Texto cifrado con KMS
 * @returns {String} Texto desencriptado
 */
KMSModule.Decrypt = async (Ciphertext) => {
    console.log('process.env.URL_KMS_VPC: ', process.env.URL_KMS_VPC);
    console.info("Retrieving information from KMS key.");
    /// Desencripta el texto proporcionado
    return await KMS.decrypt(
        {
            CiphertextBlob: Buffer.from(Ciphertext, 'base64')
        }
    ).promise().then((data) => {
        /// Devuelve la información desencriptada
        return data.Plaintext.toString('ascii');
    }).catch((err) => {
        /// Sí se encuentra algún error
        console.error('KMS.Decrypt error: ', err);
        throw err;

    });
};
/// Exporta el manager
module.exports = KMSModule;